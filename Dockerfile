FROM alpine:3.8

#Install dependencies
RUN apk --no-cache upgrade
RUN apk add --no-cache \
    apache2 apache2-ssl git php7 php7-tokenizer php7-ctype php7-session php7-apache2 \
    php7-json php7-pdo php7-pdo_mysql php7-curl php7-ldap php7-openssl php7-iconv \
    php7-xml php7-xsl php7-gd php7-zip php7-soap php7-mbstring php7-zlib php7-phar php7-fileinfo \
    php7-mysqli php7-sockets php7-xmlreader perl mysql-client tar curl imagemagick-dev \
    php7-simplexml php7-xmlwriter

WORKDIR /var/www/localhost/htdocs

COPY openemr-5_0_1_3.tar.gz .

RUN tar --absolute-names -zxf openemr-5_0_1_3.tar.gz && rm openemr-5_0_1_3.tar.gz && mv openemr-5_0_1_3 openemr

WORKDIR /var/www/localhost/htdocs

RUN chmod 666 openemr/sites/default/sqlconf.php \
    && chmod 666 openemr/interface/modules/zend_modules/config/application.config.php \
    && chown -R apache openemr/ \
    && mv openemr /var/www/localhost/htdocs/ \
    && apk del --no-cache git build-base libffi-dev python-dev

VOLUME [ "/var/www/localhost/htdocs/openemr/sites", "/etc/letsencrypt/", "/etc/ssl" ]

#configure apache & php properly
ENV APACHE_LOG_DIR=/var/log/apache2
COPY php.ini /etc/php7/php.ini
COPY openemr.conf /etc/apache2/conf.d/

#add runner and auto_configure and prevent auto_configure from being run w/o being enabled
COPY run_openemr.sh autoconfig.sh auto_configure.php /var/www/localhost/htdocs/openemr/

WORKDIR /var/www/localhost/htdocs/openemr/

RUN chmod 500 run_openemr.sh autoconfig.sh \
    && chmod 000 auto_configure.php

#fix issue with apache2 dying prematurely
RUN mkdir -p /run/apache2

#go
CMD [ "./run_openemr.sh" ]

EXPOSE 80