# vulnerable-openemr

A Docker instance of a known-vulnerable OpenEMR for penetration testing purposes.

See the [accompanying blogpost](https://atha.io/post/2019-08-16-defcon-bhv-ctf-writeup/#openemr).
